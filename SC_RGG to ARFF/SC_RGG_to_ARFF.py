atts = dict ([('Z', ['ZergPool',
                    'ZergOverlord',
                    'ZergZergling',
                    'ZergZerglingSpeed',
                    'ZergCrackling',
                    'ZergBurrow',
                    'ZergGas',
                    'ZergSecondGas',
                    'ZergSecondHatch',
                    'ZergThirdHatch',
                    'ZergFourthHatch',
                    'ZergFifthHatch',
                    'ZergSixthHatch',
                    'ZergSeventhHatch',
                    'ZergHydraDen',
                    'ZergHydra',
                    'ZergHydraRange',
                    'ZergHydraSpeed',
                    'ZergLurker',
                    'ZergLair',
                    'ZergSpire',
                    'ZergMutalisk',
                    'ZergScourge',
                    'ZergAirAttack',
                    'ZergAirArmor',
                    'ZergQueenNest',
                    'ZergQueen',
                    'ZergHive',
                    'ZergGreaterSpire',
                    'ZergGardian',
                    'ZergDevour',
                    'ZergUltraliskCavern',
                    'ZergUltralisk',
                    'ZergUltraliskArmor',
                    'ZergUltraliskSpeed',
                    'ZergDefilerMound',
                    'ZergDefiler',
                    'ZergConsume',
                    'ZergevoDen',
                    'ZergMeleeWeapons1',
                    'ZergRangeWeapons1',
                    'ZergGroundArmor1',
                    'ZergMeleeWeapons2',
                    'ZergRangeWeapons2',
                    'ZergGroundArmor2',
                    'ZergCreep',
                    'ZergSunken',
                    'ZergSpore']),
              ('T', ['TerranDepot',
                    'TerranGas',
                    'TerranSecondGas',
                    'TerranExpansion',
                    'TerranSecondExpansion',
                    'TerranThirdExpansion',
                    'TerranFourthExpansion',
                    'TerranBarracks',
                    'TerranSecondBarracks',
                    'TerranThirdBarracks',
                    'TerranMarine',
                    'TerranMedic',
                    'TerranFirebat',
                    'TerranBunker',
                    'TerranAcademy',
                    'TerranStem',
                    'TerranComsat',
                    'TerranEbay',
                    'TerranTurret',
                    'TerranInfantryAttack1',
                    'TerranInfantryArmor1',
                    'TerranInfantryAttack2',
                    'TerranInfantryArmor2',
                    'TerranFactory',
                    'TerranSecondFactory',
                    'TerranThirdFactory',
                    'TerranMachineShop',
                    'TerranSiege',
                    'TerranMines',
                    'TerranVulture',
                    'TerranTank',
                    'TerranGoliath',
                    'TerranArmory',
                    'TerranSecondArmory',
                    'TerranMetalAttack1',
                    'TerranMetalArmor1',
                    'TerranMetalAttack2',
                    'TerranMetalArmor2',
                    'TerranStarport',
                    'TerranControlTower',
                    'TerranWraith',
                    'TerranCloak',
                    'TerranDropship',
                    'TerranValkyrie',
                    'TerranScienceFacility',
                    'TerranVessel',
                    'TerranGhost',
                    'TerranNuke',
                    'TerranBattlecruiser',
                    'TerranAirAttack',
                    'TerranAirArmor']),
              ('P', ['ProtossPylon',
                    'ProtossSecondPylon',
                    'ProtossFirstGas',
                    'ProtossSecondGas',
                    'ProtossFirstExpansion',
                    'ProtossSecondExpansion',
                    'ProtossThirdExpansion',
                    'ProtossFourthExpansion',
                    'ProtossGateway',
                    'ProtossSecondGatway',
                    'ProtossThirdGatway',
                    'ProtossFourthGatway',
                    'ProtossCore',
                    'ProtossZealot',
                    'ProtossGoon',
                    'ProtossRange',
                    'ProtossForge',
                    'ProtossCannon',
                    'ProtossGroundWeapons1',
                    'ProtossGroundArmor1',
                    'ProtossShields1',
                    'ProtossGroundWeapons2',
                    'ProtossGroundArmor2',
                    'ProtossShields2',
                    'ProtossCitadel',
                    'ProtossLegs',
                    'ProtossArchives',
                    'ProtossTemplar',
                    'ProtossArchon',
                    'ProtossStorm',
                    'ProtossDarkTemplar',
                    'ProtossDarkArchon',
                    'ProtossMaelstorm',
                    #'ProtossRoboBay',
                    'ProtossShuttle',
                    'ProtossShuttleSpeed',
                    'ProtossRoboSupport',
                    'ProtossReavor',
                    'ProtossReavorDamage',
                    'ProtossReavorCapacity',
                    'ProtossObservory',
                    'ProtossObs',
                    'ProtossStargate',
                    'ProtossCorsair',
                    'ProtossDisruptionWeb',
                    'ProtossFleetBeason',
                    'ProtossCarrier',
                    'ProtossCarrierCapacity',
                    'ProtossTribunal',
                    'ProtossArbitor',
                    'ProtossStatis',
                    'ProtossRecall',
                    'ProtossAirWeapons1',
                    'ProtossAirArmor1',
                    'ProtossAirWeapons2',
                    'ProtossAirArmor2'
                     ])
            ])

supplyMult = 50
gasMult = 4/3

def CountPrice(mineral, gas, supply):
    return int(mineral + gasMult * gas + supplyMult * supply)

prices = dict([
              ('Protoss Probe', CountPrice(50, 0, 1)), 
              ('Protoss Zealot', CountPrice(100, 0, 2)), 
              ('Protoss Dragoon', CountPrice(125, 50, 2)),
              ('Protoss High Templar', CountPrice(50, 150, 2)),
              ('Protoss Dark Templar', CountPrice(125, 100, 2)),
              ('Protoss Reaver', CountPrice(200, 100, 4)),
              ('Protoss Scarab', CountPrice(15, 0, 0)),
              ('Protoss Archon', CountPrice(100, 300, 4)),
              ('Protoss Dark Archon', CountPrice(250, 200, 4)),
              ('Protoss Observer', CountPrice(25, 75, 1)),
              ('Protoss Shuttle', CountPrice(200, 0, 2)),
              ('Protoss Scout', CountPrice(275, 125, 3)),
              ('Protoss Carrier', CountPrice(350, 250, 6)),
              ('Protoss Interceptor', CountPrice(25, 0, 0)),
              ('Protoss Arbiter', CountPrice(100, 350, 4)),
              ('Protoss Corsair', CountPrice(150, 100, 2)),
              ('Terran SCV', CountPrice(50, 0, 1)),
              ('Terran Marine', CountPrice(50, 0, 1)),
              ('Terran Firebat', CountPrice(50, 25, 1)),
              ('Terran Medic', CountPrice(50, 25, 1)),
              ('Terran Ghost', CountPrice(25, 75, 1)),
              ('Terran Vulture', CountPrice(75, 0, 2)),
              ('Terran Vulture Spider Mine', CountPrice(0, 0, 0)),
              ('Terran Siege Tank Tank Mode', CountPrice(150, 100, 2)),
              ('Terran Siege Tank Siege Mode', CountPrice(150, 100, 2)),
              ('Terran Goliath', CountPrice(100, 50, 2)),
              ('Terran Wraith', CountPrice(150, 100, 2)),
              ('Terran Dropship', CountPrice(100, 100, 2)),
              ('Terran Science Vessel', CountPrice(100, 225, 2)),
              ('Terran Battlecruiser', CountPrice(400, 300, 6)),
              ('Terran Valkyrie', CountPrice(250, 125, 3)),
              ('Zerg Larva', CountPrice(0, 0, 0)),
              ('Zerg Egg', CountPrice(0, 0, 0)),
              ('Zerg Drone', CountPrice(50, 0, 1)),
              ('Zerg Zergling', CountPrice(25, 0, 0.5)),
              ('Zerg Hydralisk', CountPrice(75, 25, 1)),
              ('Zerg Lurker', CountPrice(75+50, 25+100, 2)),
              ('Zerg Lurker Egg', CountPrice(0, 0, 0)),
              ('Zerg Ultralisk', CountPrice(200, 200, 4)),
              ('Zerg Defiler', CountPrice(50, 150, 2)),
              ('Zerg Infested Terran', CountPrice(100, 50, 1)),
              ('Zerg Broodling', CountPrice(0, 0, 0)),
              ('Zerg Overlord', CountPrice(100, 0, 0)),
              ('Zerg Mutalisk', CountPrice(100, 100, 2)),
              ('Zerg Cocoon', CountPrice(0, 0, 0)),
              ('Zerg Scourge', CountPrice(25/2, 75/2, 0.5)),
              ('Zerg Queen', CountPrice(100, 100, 2)),
              ('Zerg Guardian', CountPrice(100+50, 100+100, 2)),
              ('Zerg Devourer', CountPrice(100+150, 100+50, 2)),
              ('Zerg Infested Command Center', CountPrice(0, 0, 0))
              ])

import re, glob, os, time, exceptions

folders = ['PvP', 'PvT', 'PvZ', 'TvT', 'TvZ', 'ZvZ']
prefix = r'd:\__SC_Replays_analysis\TLGGICCUP_gosu_data\\'
statsPrefix = r'stats\\'
outPrefix = r'arff\\'
ext = '.arff'

def getTimeNow():
    return time.strftime("%H:%M:%S", time.localtime())

def GetAttProtoss(building, buildings):
    if building == 'Protoss Pylon':
        if 'ProtossPylon' not in buildings.keys():
            return 'ProtossPylon'
        elif 'ProtossSecondPylon' not in buildings.keys():
            return 'ProtossSecondPylon'
    elif building == 'Protoss Assimilator':
        if 'ProtossFirstGas' not in buildings.keys():
            return 'ProtossFirstGas'
        elif 'ProtossSecondGas' not in buildings.keys():
            return 'ProtossSecondGas'
    elif building == 'Protoss Nexus':
        if 'ProtossFirstExpansion' not in buildings.keys():
            return 'ProtossFirstExpansion'
        if 'ProtossSecondExpansion' not in buildings.keys():
            return 'ProtossSecondExpansion'
        if 'ProtossThirdExpansion' not in buildings.keys():
            return 'ProtossThirdExpansion'
        if 'ProtossFourthExpansion' not in buildings.keys():
            return 'ProtossFourthExpansion'
    elif building == 'Protoss Gateway':
        if 'ProtossGateway' not in buildings.keys():
            return 'ProtossGateway'
        if 'ProtossSecondGatway' not in buildings.keys():
            return 'ProtossSecondGatway'
        if 'ProtossThirdGatway' not in buildings.keys():
            return 'ProtossThirdGatway'
        if 'ProtossFourthGatway' not in buildings.keys():
            return 'ProtossFourthGatway'
    elif building == 'Protoss Cybernetics Core':
        if 'ProtossCore' not in buildings.keys():
            return 'ProtossCore'
    elif building == 'Protoss Zealot':
        if 'ProtossZealot' not in buildings.keys():
            return 'ProtossZealot'
    elif building == 'Protoss Dragoon':
        if 'ProtossGoon' not in buildings.keys():
            return 'ProtossGoon'
    elif building == 'Singularity Charge':
        if 'ProtossRange' not in buildings.keys():
            return 'ProtossRange'
    elif building == 'Protoss Forge':
        if 'ProtossForge' not in buildings.keys():
            return 'ProtossForge'
    elif building == 'Protoss Photon Cannon':
        if 'ProtossCannon' not in buildings.keys():
            return 'ProtossCannon'
    elif building == 'Protoss Ground Weapons':
        if 'ProtossGroundWeapons1' not in buildings.keys():
            return 'ProtossGroundWeapons1'
        if 'ProtossGroundWeapons2' not in buildings.keys():
            return 'ProtossGroundWeapons2'
    elif building == 'Protoss Ground Armor':
        if 'ProtossGroundArmor1' not in buildings.keys():
            return 'ProtossGroundArmor1'
        if 'ProtossGroundArmor2' not in buildings.keys():
            return 'ProtossGroundArmor2'
    elif building == 'Protoss Plasma Shields':
        if 'ProtossShields1' not in buildings.keys():
            return 'ProtossShields1'
        if 'ProtossShields2' not in buildings.keys():
            return 'ProtossShields2'
    elif building == 'Protoss Citadel of Adun':
        if 'ProtossCitadel' not in buildings.keys():
            return 'ProtossCitadel'
    elif building == 'Leg Enhancements':
        if 'ProtossLegs' not in buildings.keys():
            return 'ProtossLegs'
    elif building == 'Protoss Templar Archives':
        if 'ProtossArchives' not in buildings.keys():
            return 'ProtossArchives'
    elif building == 'Protoss High Templar':
        if 'ProtossTemplar' not in buildings.keys():
            return 'ProtossTemplar'
    elif building == 'Protoss Archon':
        if 'ProtossArchon' not in buildings.keys():
            return 'ProtossArchon'
    elif building == 'Psionic Storm':
        if 'ProtossStorm' not in buildings.keys():
            return 'ProtossStorm'
    elif building == 'Protoss Dark Templar':
        if 'ProtossDarkTemplar' not in buildings.keys():
            return 'ProtossDarkTemplar'
    elif building == 'Protoss Dark Archon':
        if 'ProtossDarkArchon' not in buildings.keys():
            return 'ProtossDarkArchon'
    elif building == 'Maelstrom':
        if 'ProtossMaelstorm' not in buildings.keys():
            return 'ProtossMaelstorm'
    elif building == 'Protoss Shuttle':
        if 'ProtossShuttle' not in buildings.keys():
            return 'ProtossShuttle'
    elif building == 'Gravitic Drive':
        if 'ProtossShuttleSpeed' not in buildings.keys():
            return 'ProtossShuttleSpeed'
    elif building == 'Protoss Robotics Support Bay':
        if 'ProtossRoboSupport' not in buildings.keys():
            return 'ProtossRoboSupport'
    elif building == 'Protoss Reaver':
        if 'ProtossReavor' not in buildings.keys():
            return 'ProtossReavor'
    elif building == 'Scarab Damage':
        if 'ProtossReavorDamage' not in buildings.keys():
            return 'ProtossReavorDamage'
    elif building == 'Reaver Capacity':
        if 'ProtossReavorCapacity' not in buildings.keys():
            return 'ProtossReavorCapacity'
    elif building == 'Protoss Observatory':
        if 'ProtossObservory' not in buildings.keys():
            return 'ProtossObservory'
    elif building == 'Protoss Observer':
        if 'ProtossObs' not in buildings.keys():
            return 'ProtossObs'
    elif building == 'Protoss Stargate':
        if 'ProtossStargate' not in buildings.keys():
            return 'ProtossStargate'
    elif building == 'Protoss Corsair':
        if 'ProtossCorsair' not in buildings.keys():
            return 'ProtossCorsair'
    elif building == 'Disruption Web':
        if 'ProtossDisruptionWeb' not in buildings.keys():
            return 'ProtossDisruptionWeb'
    elif building == 'Protoss Fleet Beacon':
        if 'ProtossFleetBeason' not in buildings.keys():
            return 'ProtossFleetBeason'
    elif building == 'Protoss Carrier':
        if 'ProtossCarrier' not in buildings.keys():
            return 'ProtossCarrier'
    elif building == 'Carrier Capacity':
        if 'ProtossCarrierCapacity' not in buildings.keys():
            return 'ProtossCarrierCapacity'
    elif building == 'Protoss Arbiter Tribunal':
        if 'ProtossTribunal' not in buildings.keys():
            return 'ProtossTribunal'
    elif building == 'Protoss Arbiter':
        if 'ProtossArbitor' not in buildings.keys():
            return 'ProtossArbitor'
    elif building == 'Stasis Field':
        if 'ProtossStatis' not in buildings.keys():
            return 'ProtossStatis'
    elif building == 'Recall':
        if 'ProtossRecall' not in buildings.keys():
            return 'ProtossRecall'
    elif building == 'Protoss Air Weapons':
        if 'ProtossAirWeapons1' not in buildings.keys():
            return 'ProtossAirWeapons1'
        if 'ProtossAirWeapons2' not in buildings.keys():
            return 'ProtossAirWeapons2'
    elif building == 'Protoss Air Armor':
        if 'ProtossAirArmor1' not in buildings.keys():
            return 'ProtossAirArmor1'
        if 'ProtossAirArmor2' not in buildings.keys():
            return 'ProtossAirArmor2'
    return ''
def GetAttTerran(building, buildings):
    if building == 'Terran Supply Depot':
        if 'TerranDepot' not in buildings.keys():
            return 'TerranDepot'
    elif building == 'Terran Refinery':
        if 'TerranGas' not in buildings.keys():
            return 'TerranGas'
        elif 'TerranSecondGas' not in buildings.keys():
            return 'TerranSecondGas'
    elif building == 'Terran Command Center':
        if 'TerranExpansion' not in buildings.keys():
            return 'TerranExpansion'
        elif 'TerranSecondExpansion' not in buildings.keys():
            return 'TerranSecondExpansion'
        elif 'TerranThirdExpansion' not in buildings.keys():
            return 'TerranThirdExpansion'
        elif 'TerranFourthExpansion' not in buildings.keys():
            return 'TerranFourthExpansion'
    elif building == 'Terran Barracks':
        if 'TerranBarracks' not in buildings.keys():
            return 'TerranBarracks'
        elif 'TerranSecondBarracks' not in buildings.keys():
            return 'TerranSecondBarracks'
        elif 'TerranThirdBarracks' not in buildings.keys():
            return 'TerranThirdBarracks'
    elif building == 'Terran Marine':
        if 'TerranMarine' not in buildings.keys():
            return 'TerranMarine'
    elif building == 'Terran Medic':
        if 'TerranMedic' not in buildings.keys():
            return 'TerranMedic'
    elif building == 'Terran Firebat':
        if 'TerranFirebat' not in buildings.keys():
            return 'TerranFirebat'
    elif building == 'Terran Bunker':
        if 'TerranBunker' not in buildings.keys():
            return 'TerranBunker'
    elif building == 'Terran Academy':
        if 'TerranAcademy' not in buildings.keys():
            return 'TerranAcademy'
    elif building == 'Stim Packs':
        if 'TerranStem' not in buildings.keys():
            return 'TerranStem'
    elif building == 'Terran Comsat Station':
        if 'TerranComsat' not in buildings.keys():
            return 'TerranComsat'
    elif building == 'Terran Engineering Bay':
        if 'TerranEbay' not in buildings.keys():
            return 'TerranEbay'
    elif building == 'Terran Missile Turret':
        if 'TerranTurret' not in buildings.keys():
            return 'TerranTurret'
    elif building == 'Terran Infantry Weapons':
        if 'TerranInfantryAttack1' not in buildings.keys():
            return 'TerranInfantryAttack1'
        elif 'TerranInfantryAttack2' not in buildings.keys():
            return 'TerranInfantryAttack2'
    elif building == 'Terran Infantry Armor':
        if 'TerranInfantryArmor1' not in buildings.keys():
            return 'TerranInfantryArmor1'
        elif 'TerranInfantryArmor2' not in buildings.keys():
            return 'TerranInfantryArmor2'
    elif building == 'Terran Factory':
        if 'TerranFactory' not in buildings.keys():
            return 'TerranFactory'
        elif 'TerranSecondFactory' not in buildings.keys():
            return 'TerranSecondFactory'
        elif 'TerranThirdFactory' not in buildings.keys():
            return 'TerranThirdFactory'
    elif building == 'Terran Machine Shop':
        if 'TerranMachineShop' not in buildings.keys():
            return 'TerranMachineShop'
    elif building == 'Tank Siege Mode':
        if 'TerranSiege' not in buildings.keys():
            return 'TerranSiege'
    elif building == 'Spider Mines':
        if 'TerranMines' not in buildings.keys():
            return 'TerranMines'
    elif building == 'Terran Vulture':
        if 'TerranVulture' not in buildings.keys():
            return 'TerranVulture'
    elif building == 'Terran Siege Tank Tank Mode':
        if 'TerranTank' not in buildings.keys():
            return 'TerranTank'
    elif building == 'Terran Goliath':
        if 'TerranGoliath' not in buildings.keys():
            return 'TerranGoliath'
    elif building == 'Terran Armory':
        if 'TerranArmory' not in buildings.keys():
            return 'TerranArmory'
        elif 'TerranSecondArmory' not in buildings.keys():
            return 'TerranSecondArmory'
    elif building == 'Terran Vehicle Weapons':
        if 'TerranMetalAttack1' not in buildings.keys():
            return 'TerranMetalAttack1'
        elif 'TerranMetalAttack2' not in buildings.keys():
            return 'TerranMetalAttack2'
    elif building == 'Terran Vehicle Plating':
        if 'TerranMetalArmor1' not in buildings.keys():
            return 'TerranMetalArmor1'
        elif 'TerranMetalArmor2' not in buildings.keys():
            return 'TerranMetalArmor2'
    elif building == 'Terran Starport':
        if 'TerranStarport' not in buildings.keys():
            return 'TerranStarport'
    elif building == 'Terran Control Tower':
        if 'TerranControlTower' not in buildings.keys():
            return 'TerranControlTower'
    elif building == 'Terran Wraith':
        if 'TerranWraith' not in buildings.keys():
            return 'TerranWraith'
    elif building == 'Cloaking Field': # for wraith
        if 'TerranCloak' not in buildings.keys():
            return 'TerranCloak'
    elif building == 'Terran Dropship':
        if 'TerranDropship' not in buildings.keys():
            return 'TerranDropship'
    elif building == 'Terran Valkyrie':
        if 'TerranValkyrie' not in buildings.keys():
            return 'TerranValkyrie'
    elif building == 'Terran Science Facility':
        if 'TerranScienceFacility' not in buildings.keys():
            return 'TerranScienceFacility'
    elif building == 'Terran Science Vessel':
        if 'TerranVessel' not in buildings.keys():
            return 'TerranVessel'
    elif building == 'Terran Ghost':
        if 'TerranGhost' not in buildings.keys():
            return 'TerranGhost'
    elif building == 'Terran Nuclear Silo':
        if 'TerranNuke' not in buildings.keys():
            return 'TerranNuke'
    elif building == 'Terran Battlecruiser':
        if 'TerranBattlecruiser' not in buildings.keys():
            return 'TerranBattlecruiser'
    elif building == 'Terran Ship Weapons':
        if 'TerranAirAttack' not in buildings.keys():
            return 'TerranAirAttack'
    elif building == 'Terran Ship Plating':
        if 'TerranAirArmor' not in buildings.keys():
            return 'TerranAirArmor'
def GetAttZerg(building, buildings):
    if building == 'Zerg Spawning Pool':
        if 'ZergPool' not in buildings.keys():
            return 'ZergPool'
    if building == 'Zerg Overlord':
        if 'ZergOverlord' not in buildings.keys():
            return 'ZergOverlord'
    if building == 'Zerg Zergling':
        if 'ZergZergling' not in buildings.keys():
            return 'ZergZergling'
    if building == 'Metabolic Boost':
        if 'ZergZerglingSpeed' not in buildings.keys():
            return 'ZergZerglingSpeed'
    if building == 'Adrenal Glands':
        if 'ZergCrackling' not in buildings.keys():
            return 'ZergCrackling'
    if building == 'Burrowing':
        if 'ZergBurrow' not in buildings.keys():
            return 'ZergBurrow'
    if building == 'Zerg Extractor':
        if 'ZergGas' not in buildings.keys():
            return 'ZergGas'
        elif 'ZergSecondGas' not in buildings.keys():
            return 'ZergSecondGas'
    if building == 'Zerg Hatchery':
        if 'ZergSecondHatch' not in buildings.keys():
            return 'ZergSecondHatch'
        elif 'ZergThirdHatch' not in buildings.keys():
            return 'ZergThirdHatch'
        elif 'ZergFourthHatch' not in buildings.keys():
            return 'ZergFourthHatch'
        elif 'ZergFifthHatch' not in buildings.keys():
            return 'ZergFifthHatch'
        elif 'ZergSixthHatch' not in buildings.keys():
            return 'ZergSixthHatch'
        elif 'ZergSeventhHatch' not in buildings.keys():
            return 'ZergSeventhHatch'
    if building == 'Zerg Hydralisk Den':
        if 'ZergHydraDen' not in buildings.keys():
            return 'ZergHydraDen'
    if building == 'Zerg Hydralisk':
        if 'ZergHydra' not in buildings.keys():
            return 'ZergHydra'
    if building == 'Grooved Spines':
        if 'ZergHydraRange' not in buildings.keys():
            return 'ZergHydraRange'
    if building == 'Muscular Augments':
        if 'ZergHydraSpeed' not in buildings.keys():
            return 'ZergHydraSpeed'
    if building == 'Zerg Lurker':
        if 'ZergLurker' not in buildings.keys():
            return 'ZergLurker'
    if building == 'Zerg Lair':
        if 'ZergLair' not in buildings.keys():
            return 'ZergLair'
    if building == 'Zerg Spire':
        if 'ZergSpire' not in buildings.keys():
            return 'ZergSpire'
    if building == 'Zerg Mutalisk':
        if 'ZergMutalisk' not in buildings.keys():
            return 'ZergMutalisk'
    if building == 'Zerg Scourge':
        if 'ZergScourge' not in buildings.keys():
            return 'ZergScourge'
    if building == 'Zerg Flyer Attacks':
        if 'ZergAirAttack' not in buildings.keys():
            return 'ZergAirAttack'
    if building == 'Zerg Flyer Carapace':
        if 'ZergAirArmor' not in buildings.keys():
            return 'ZergAirArmor'
    if building == 'Zerg Queens Nest':
        if 'ZergQueenNest' not in buildings.keys():
            return 'ZergQueenNest'
    if building == 'Zerg Queen':
        if 'ZergQueen' not in buildings.keys():
            return 'ZergQueen'
    if building == 'Zerg Hive':
        if 'ZergHive' not in buildings.keys():
            return 'ZergHive'
    if building == 'Zerg Greater Spire':
        if 'ZergGreaterSpire' not in buildings.keys():
            return 'ZergGreaterSpire'
    if building == 'Zerg Guardian':
        if 'ZergGardian' not in buildings.keys():
            return 'ZergGardian'
    if building == 'Zerg Devour': # devour - what is this?
        if 'ZergDevour' not in buildings.keys():
            return 'ZergDevour'
    if building == 'Zerg Ultralisk Cavern':
        if 'ZergUltraliskCavern' not in buildings.keys():
            return 'ZergUltraliskCavern'
    if building == 'Zerg Ultralisk':
        if 'ZergUltralisk' not in buildings.keys():
            return 'ZergUltralisk'
    if building == 'Chitinous Plating':
        if 'ZergUltraliskArmor' not in buildings.keys():
            return 'ZergUltraliskArmor'
    if building == 'Ultra Speed': # ultralisk speed - what is this?
        if 'ZergUltraliskSpeed' not in buildings.keys():
            return 'ZergUltraliskSpeed'
    if building == 'Zerg Defiler Mound':
        if 'ZergDefilerMound' not in buildings.keys():
            return 'ZergDefilerMound'
    if building == 'Zerg Defiler':
        if 'ZergDefiler' not in buildings.keys():
            return 'ZergDefiler'
    if building == 'Consume':
        if 'ZergConsume' not in buildings.keys():
            return 'ZergConsume'
    if building == 'Zerg Evolution Chamber':
        if 'ZergevoDen' not in buildings.keys():
            return 'ZergevoDen'
    if building == 'Zerg Melee Attacks':
        if 'ZergMeleeWeapons1' not in buildings.keys():
            return 'ZergMeleeWeapons1'
        elif 'ZergMeleeWeapons2' not in buildings.keys():
            return 'ZergMeleeWeapons2'
    if building == 'Zerg Missile Attacks':
        if 'ZergRangeWeapons1' not in buildings.keys():
            return 'ZergRangeWeapons1'
        elif 'ZergRangeWeapons2' not in buildings.keys():
            return 'ZergRangeWeapons2'
    if building == 'Zerg Carapace':
        if 'ZergGroundArmor1' not in buildings.keys():
            return 'ZergGroundArmor1'
        elif 'ZergGroundArmor2' not in buildings.keys():
            return 'ZergGroundArmor2'
    if building == 'Zerg Creep Colony':
        if 'ZergCreep' not in buildings.keys():
            return 'ZergCreep'
    if building == 'Zerg Sunken Colony':
        if 'ZergSunken' not in buildings.keys():
            return 'ZergSunken'
    if building == 'Zerg Spore Colony':
        if 'ZergSpore' not in buildings.keys():
            return 'ZergSpore'

def GetAttribute(race, building, buildings):
    if race == 'Protoss':
        return GetAttProtoss(building, buildings)
    elif race == 'Terran':
        return GetAttTerran(building, buildings)
    elif race == 'Zerg':
        return GetAttZerg(building, buildings)
    raise AttributeError("Unknown race")
    
repCount = 0
firstEver = True
def ProcessReplay(replay, mu):
    #print("--- Processing replay")
    global repCount
    repCount += 1
    if repCount % 100 == 0:
        print(getTimeNow() + " - " + str(repCount) + " done")
    wholeFile = replay.read()
    # Basic info
    repId = re.search(r'RepPath: .*\\(.*).rep', wholeFile).group(1)
    map = re.search('MapName: (.*)', wholeFile).group(1)
    p0 = re.search('0, .*, (.*), .*', wholeFile).group(1)
    p1 = re.search('1, .*, (.*), .*', wholeFile).group(1)
    players = [p0, p1]
    buildings = [{}, {}]
    damage = [0,0]
    # Go through the file line by line
    replay.seek(0)
    for line in replay.readlines():
        # Count damage
        match = re.search('[0-9]*,([01]),Destroyed,[0-9]*,([A-Z a-z]*),.*', line)
        if match is not None:
            unitName = match.group(2)
            playerNo = int(match.group(1))
            if unitName in prices.keys():
                damage[playerNo] += prices[unitName]
            #else:  I think I've debugged it enough
            #    if 'Terran' in unitName or 'Zerg' in unitName or 'Protoss' in unitName:
            #        print('---- NOT FOUND: ' + unitName)
            continue
        
        # Buildings and units
        match = re.search('([1-9]+[0-9]*),([01]),(Created|Morph),[0-9]*,([A-Z a-z]*),.*', line)
        # Upgrades
        if match is None:
            match = re.search('([1-9]+[0-9]*),([01]),(FinishUpgrade|FinishResearch),([A-Z a-z0-9]*).*', line)
        if match is not None:
            buildingName = match.group(4)
            playerNo = int(match.group(2))
            frame = int(match.group(1))
            attName = GetAttribute(players[playerNo], buildingName, buildings[playerNo])
            if attName not in buildings[playerNo].keys():
                buildings[playerNo][attName] = (frame, damage[0] + damage[1])
            continue
    
    # Output the results
    withDamage = False
    for i in {0, 1}:
        game = ""
        first = True
        for a in atts[players[i][0:1]]:
            if not first:
                game += ","
            else:
                first = False
            if withDamage:
                if a in buildings[i].keys():
                    game += str(buildings[i][a][0]) + ";" + str(buildings[i][a][1])
                else:
                    game += "-1;-1"
            else:
                if a in buildings[i].keys():
                    game += str(buildings[i][a][0])
                else:
                    game += "-1"
        with open(statsPrefix + mu + '_' + players[i] + ext, "a+") as file:
            # Add one more empty attribute for labeling purpose
            file.write(game + ",\n")

def ProcessFolder(folder):
    global repCount
    repCount = 0
    print(getTimeNow() + ' Processing folder: ' + folder)
    path = prefix + folder;
    for file in glob.glob(os.path.join(path, '*.rgd')):
        with open(file) as f:
            ProcessReplay(f, folder)
    print(getTimeNow() + " - " + str(repCount) + " total done")

def WriteHeader(file, matchup, side):
    file.write("@relation Starcraft_{0}_{1}\n\n".format(matchup, side))
    # Add all attrubutes for fetures
    for a in atts[side[0:1]]:
        file.write("@attribute {0} numeric\n".format(a))
    # Add attribute for all possible builds from clustering
    if side[0:1] == 'P':
        file.write("@attribute midBuild {two_gates,fast_dt,templar,speedzeal,corsair,nony,reaver_drop}")
    elif side[0:1] == 'T':
        file.write("@attribute midBuild {bio,rax_fe,two_facto,vultures,drop,unknown}")
    else:
        file.write("@attribute midBuild {fast_mutas,mutas,lurkers,hydras,unknown}")
    file.write("\n@data\n")

def PrepareFiles(mu):
    races = dict([('P', "Protoss"), ('T', "Terran"), ('Z', "Zerg")])
    for r in [mu[0:1], mu[2:3]]:
        filePath = statsPrefix + mu + '_' + races[r] + ext
        if os.path.isfile(filePath):
            os.remove(filePath)
        with open(filePath, 'w') as file:
            WriteHeader(file, mu, races[r])

if __name__ == '__main__':
    for folder in folders:
        PrepareFiles(folder)
        ProcessFolder(folder)